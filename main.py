import turtle

wndw = turtle.Screen()
wndw.title("Pong Game")
wndw.bgcolor("black")
wndw.setup(width=800, height=600)
wndw.tracer(0) # stops window from updating

#Main game loop
while True:
    wndw.update()